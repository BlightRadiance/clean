package testing.com.data.repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import testing.com.common.TrampolineSchedulerProviderImpl;
import testing.com.data.mapper.TranslationMapper;
import testing.com.data.model.LanguageEntity;
import testing.com.data.model.TranslationEntity;
import testing.com.data.persistence.TranslationDao;

public class TranslationRepositoryImplTest {

    @Mock
    TranslationDao mDao;

    private TranslationRepositoryImpl mTranslationRepository;

    @Spy
    private TranslationMapper mMapper;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mTranslationRepository = new TranslationRepositoryImpl(mDao, mMapper, new TrampolineSchedulerProviderImpl());
    }

    @Test
    public void shouldLoadHistory() {
        List<TranslationEntity> entities = new ArrayList<>();
        entities.add(getTestEntity(1));
        entities.add(getTestEntity(2));
        Mockito.when(mDao.getHistory()).thenReturn(Flowable.just(entities));

        mTranslationRepository.getHistory().subscribe(list -> {
                Assert.assertEquals(list.size(), entities.size());
                for (TranslationEntity entity : entities) {
                    Assert.assertTrue(list.contains(mMapper.to(entity)));
                }
        });
    }

    private TranslationEntity getTestEntity(long seed) {
        TranslationEntity result = new TranslationEntity();
        result.id = seed;
        result.inputText = "inputText" + seed;
        result.outputText = "outputText" + seed;
        result.from = new LanguageEntity();
        result.from.langId = "langId_in" + seed;
        result.from.langName = "langName_in" + seed;
        result.to = new LanguageEntity();
        result.to.langId = "langId_out" + seed;
        result.to.langName = "langName_out" + seed;
        result.mIsFavorite = seed % 2 == 0;
        return result;
    }
}
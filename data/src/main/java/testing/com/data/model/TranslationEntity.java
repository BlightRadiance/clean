package testing.com.data.model;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "translations")
public class TranslationEntity {

    @PrimaryKey(autoGenerate = true)
    public long id;

    public String inputText;
    public String outputText;

    @Embedded(prefix = "from")
    public LanguageEntity from;
    @Embedded(prefix = "to")
    public LanguageEntity to;

    public boolean mIsFavorite;
}

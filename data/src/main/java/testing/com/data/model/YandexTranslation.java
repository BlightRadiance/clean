package testing.com.data.model;

import java.util.Arrays;

public class YandexTranslation {

    public int code;
    public String lang;
    public String[] text;

    @Override
    public String toString() {
        return "YandexTranslation{" +
                "code=" + code +
                ", lang='" + lang + '\'' +
                ", text=" + Arrays.toString(text) +
                '}';
    }
}

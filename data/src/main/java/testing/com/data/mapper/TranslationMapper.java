package testing.com.data.mapper;

import testing.com.common.mapper.Mapper;
import testing.com.data.model.LanguageEntity;
import testing.com.data.model.TranslationEntity;
import testing.com.domain.model.Translation;

public class TranslationMapper implements Mapper<TranslationEntity, Translation> {

    @Override
    public Translation to(TranslationEntity entity) {
        return new Translation(entity.id,
                entity.inputText, entity.outputText,
                entity.from.langId, entity.from.langName,
                entity.to.langId, entity.to.langName,
                entity.mIsFavorite);
    }

    @Override
    public TranslationEntity from(Translation translation) {
        TranslationEntity result = new TranslationEntity();
        result.id = translation.getId();
        result.inputText = translation.getInputText();
        result.outputText = translation.getOutputText();
        result.from = new LanguageEntity();
        result.from.langId = translation.getFromLanguageId();
        result.from.langName = translation.getFromLanguageName();
        result.to = new LanguageEntity();
        result.to.langId = translation.getToLanguageId();
        result.to.langName = translation.getToLanguageName();
        result.mIsFavorite = translation.isFavorite();
        return result;
    }
}

package testing.com.data.mapper;

import javax.inject.Inject;
import javax.inject.Singleton;

import testing.com.data.model.YandexTranslation;
import testing.com.domain.model.Translation;

@Singleton
public class YandexTranslationMapper {

    @Inject
    YandexTranslationMapper() {
    }

    public Translation map(Translation input, YandexTranslation translation) {
        Translation result = input.partialCopy();
        result.setOutputText(translation.text[0]);
        return result;
    }
}

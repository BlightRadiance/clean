package testing.com.data.api;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;
import testing.com.data.model.YandexTranslation;

public interface YandexTranslateApi {

    @GET("api/v1.5/tr.json/translate")
    Single<YandexTranslation> translate(@Query("lang") String langOutOrInDashOut,
                                        @Query("text") String text,
                                        @Query("key") String apiKey);
}

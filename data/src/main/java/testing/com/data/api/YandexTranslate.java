package testing.com.data.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import testing.com.common.SchedulerProvider;
import testing.com.data.BuildConfig;
import testing.com.data.mapper.YandexTranslationMapper;
import testing.com.domain.api.TranslateApi;
import testing.com.domain.model.Translation;

@Singleton
public class YandexTranslate implements TranslateApi {

    private static final String BASE_URL = "https://translate.yandex.net/";

    private final SchedulerProvider mSchedulerProvider;
    private final YandexTranslationMapper mYandexTranslationMapper;
    private YandexTranslateApi mApi;

    @Inject
    public YandexTranslate(SchedulerProvider schedulerProvider, YandexTranslationMapper yandexTranslationMapper) {
        mSchedulerProvider = schedulerProvider;
        mYandexTranslationMapper = yandexTranslationMapper;
        mApi = initApi();
    }

    private YandexTranslateApi initApi() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient.build())
                .build();
        return retrofit.create(YandexTranslateApi.class);
    }

    @Override
    public Single<Translation> translate(Translation translation) {
        return mApi.translate(
                translation.getFromLanguageId() + "-" + translation.getToLanguageId(), translation.getInputText(), BuildConfig.YANDEX_TRANSLATE_KEY)
                .map(yandexTranslation -> mYandexTranslationMapper.map(translation, yandexTranslation))
                .subscribeOn(mSchedulerProvider.io());
    }
}

package testing.com.data.di;

import android.arch.persistence.room.Room;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import testing.com.common.SchedulerProvider;
import testing.com.data.api.YandexTranslate;
import testing.com.data.mapper.TranslationMapper;
import testing.com.data.mapper.YandexTranslationMapper;
import testing.com.data.persistence.TranslationDao;
import testing.com.data.persistence.TranslationDatabase;
import testing.com.data.repository.TranslationRepositoryImpl;
import testing.com.domain.api.TranslateApi;
import testing.com.domain.repository.TranslationRepository;

@Module
public class RepositoryModule {

    private TranslationDatabase database;

    public RepositoryModule(Context appContext) {
        database = Room.databaseBuilder(appContext,
                TranslationDatabase.class, "translations").build();
    }

    @Provides
    @Singleton
    public TranslationDao provideTranslationDao() {
        return database.translationDao();
    }

    @Provides
    @Singleton
    public TranslationMapper provideTranslationMapper() {
        return new TranslationMapper();
    }

    @Provides
    @Singleton
    public TranslationRepository provideTranslationRepository(TranslationDao dao, TranslationMapper mapper,
                                                              SchedulerProvider schedulerProvider) {
        return new TranslationRepositoryImpl(dao, mapper, schedulerProvider);
    }

    @Provides
    @Singleton
    public TranslateApi provideTranslateApi(SchedulerProvider schedulerProvider,
                                            YandexTranslationMapper yandexTranslationMapper) {
        return new YandexTranslate(schedulerProvider, yandexTranslationMapper);
    }
}

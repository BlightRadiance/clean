package testing.com.data.repository;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import testing.com.common.SchedulerProvider;
import testing.com.common.mapper.Mapper;
import testing.com.data.model.TranslationEntity;
import testing.com.data.persistence.TranslationDao;
import testing.com.domain.model.Translation;
import testing.com.domain.repository.TranslationRepository;

public class TranslationRepositoryImpl implements TranslationRepository {

    private final TranslationDao mDao;
    private final Mapper<TranslationEntity, Translation> mMapper;
    private final SchedulerProvider mSchedulers;

    public TranslationRepositoryImpl(TranslationDao dao, Mapper<TranslationEntity, Translation> mapper,
                                     SchedulerProvider schedulerProvider) {
        mDao = dao;
        mMapper = mapper;
        mSchedulers = schedulerProvider;
    }

    @Override
    public Flowable<List<Translation>> getHistory() {
        return mDao.getHistory().flatMap(translationEntities ->
                Observable.fromIterable(translationEntities)
                        .map(mMapper::to)
                        .toList()
                        .toFlowable())
                .subscribeOn(mSchedulers.io());
    }

    @Override
    public Flowable<List<Translation>> getFavorites() {
        return mDao.getHistory().flatMap(translationEntities ->
                Observable.fromIterable(translationEntities)
                        .map(mMapper::to)
                        .filter(Translation::isFavorite)
                        .toList()
                        .toFlowable())
                .subscribeOn(mSchedulers.io());
    }

    @Override
    public Completable insertTranslation(Translation translation) {
        return Completable.fromAction(() ->
                mDao.insertTranslation(mMapper.from(translation)))
                .subscribeOn(mSchedulers.io());
    }

    @Override
    public Completable updateTranslation(Translation translation) {
        return Completable.fromAction(() ->
                mDao.updateTranslation(mMapper.from(translation)))
                .subscribeOn(mSchedulers.io());
    }

    @Override
    public Completable clearHistory() {
        return Completable.fromAction(mDao::clearHistory);
    }
}

package testing.com.data.persistence;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Flowable;
import testing.com.data.model.TranslationEntity;

@Dao
public interface TranslationDao {

    @Query("SELECT * FROM translations ORDER BY id DESC")
    Flowable<List<TranslationEntity>> getHistory();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTranslation(TranslationEntity translation);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateTranslation(TranslationEntity translation);

    @Query("DELETE FROM translations")
    void clearHistory();
}

package testing.com.data.persistence;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import testing.com.data.model.TranslationEntity;

@Database(version = 1, entities = {TranslationEntity.class}, exportSchema = false)
public abstract class TranslationDatabase extends RoomDatabase {

    abstract public TranslationDao translationDao();
}

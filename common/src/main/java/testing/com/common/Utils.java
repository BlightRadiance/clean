package testing.com.common;

public class Utils {

    private Utils() {
    }

    public static boolean isEmpty(String text) {
        return text == null || text.trim().isEmpty();
    }
}

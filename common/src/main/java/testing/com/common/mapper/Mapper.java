package testing.com.common.mapper;

public interface Mapper<I, O> {
    O to(I object);
    I from(O object);
}

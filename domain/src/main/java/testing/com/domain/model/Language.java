package testing.com.domain.model;

import android.support.annotation.NonNull;

import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public class Language {

    @NonNull
    private final String mLangId;
    @NonNull
    private final String mLangName;

    public Language(String id, String name) {
        mLangId = id;
        mLangName = name;
    }

    @NonNull
    public String getId() {
        return mLangId;
    }

    @NonNull
    public String getName() {
        return mLangName;
    }

    @Override
    public String toString() {
        return mLangName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Language language = (Language) o;

        if (!mLangId.equals(language.mLangId)) return false;
        return mLangName.equals(language.mLangName);
    }

    @Override
    public int hashCode() {
        int result = mLangId.hashCode();
        result = 31 * result + mLangName.hashCode();
        return result;
    }
}

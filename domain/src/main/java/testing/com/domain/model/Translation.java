package testing.com.domain.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public class Translation {

    private long mId;

    @NonNull
    private String mInputText = "";
    @NonNull
    private String mOutputText = "";

    @Nullable
    private Language mFrom;
    @Nullable
    private Language mTo;

    private boolean mIsFavorite;

    public Translation() {
    }

    public Translation(String inputText) {
        mInputText = inputText;
    }

    public Translation(long id, String inputText, String outputText, String fromLangId,
                       String fromLangName, String toLangId, String toLangName,
                       boolean isFavorite) {
        mId = id;
        mInputText = inputText;
        mOutputText = outputText;
        mFrom = new Language(fromLangId, fromLangName);
        mTo = new Language(toLangId, toLangName);
        mIsFavorite = isFavorite;
    }

    public long getId() {
        return mId;
    }

    public void setInputText(String inputText) {
        mInputText = inputText;
    }

    public void setOutputText(String text) {
        mOutputText = text;
    }

    public void setFromLanguage(@Nullable Language from) {
        mFrom = from;
    }

    public void setToLanguage(@Nullable Language to) {
        mTo = to;
    }

    @NonNull
    public String getFromLanguageId() {
        return mFrom != null ? mFrom.getId() : "";
    }

    @NonNull
    public String getToLanguageId() {
        return mTo != null ? mTo.getId() : "";
    }

    @NonNull
    public String getFromLanguageName() {
        return mFrom != null ? mFrom.getName() : "";
    }

    @NonNull
    public String getToLanguageName() {
        return mTo != null ? mTo.getName() : "";
    }

    @NonNull
    public String getInputText() {
        return mInputText;
    }

    @NonNull
    public String getOutputText() {
        return mOutputText;
    }

    public void setFavorite(boolean value) {
        mIsFavorite = value;
    }

    public boolean isFavorite() {
        return mIsFavorite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Translation that = (Translation) o;

        if (mId != that.mId) return false;
        if (mIsFavorite != that.mIsFavorite) return false;
        if (!mInputText.equals(that.mInputText)) return false;
        if (!mOutputText.equals(that.mOutputText)) return false;
        if (mFrom != null ? !mFrom.equals(that.mFrom) : that.mFrom != null) return false;
        return mTo != null ? mTo.equals(that.mTo) : that.mTo == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (mId ^ (mId >>> 32));
        result = 31 * result + mInputText.hashCode();
        result = 31 * result + mOutputText.hashCode();
        result = 31 * result + (mFrom != null ? mFrom.hashCode() : 0);
        result = 31 * result + (mTo != null ? mTo.hashCode() : 0);
        result = 31 * result + (mIsFavorite ? 1 : 0);
        return result;
    }

    public Translation partialCopy() {
        Translation translation = new Translation();
        translation.mInputText = mInputText;
        translation.mTo = mTo;
        translation.mFrom = mFrom;
        return translation;
    }
}

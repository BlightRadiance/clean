package testing.com.domain.repository;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import testing.com.domain.model.Translation;

public interface TranslationRepository {

    Flowable<List<Translation>> getHistory();

    Flowable<List<Translation>> getFavorites();

    Completable insertTranslation(Translation translation);

    Completable updateTranslation(Translation translation);

    Completable clearHistory();
}

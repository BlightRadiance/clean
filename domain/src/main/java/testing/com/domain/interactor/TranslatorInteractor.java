package testing.com.domain.interactor;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import testing.com.domain.api.TranslateApi;
import testing.com.domain.model.Language;
import testing.com.domain.model.Translation;
import testing.com.domain.repository.TranslationRepository;

@ParametersAreNonnullByDefault
public class TranslatorInteractor {

    @NonNull
    private final TranslationRepository mRepository;

    @NonNull
    private final TranslateApi mTranslateApi;

    @Inject
    TranslatorInteractor(TranslationRepository repository, TranslateApi translateApi) {
        mRepository = repository;
        mTranslateApi = translateApi;
    }

    @NonNull
    public Flowable<List<Translation>> getTranslationHistory() {
        return mRepository.getHistory();
    }

    @NonNull
    public Flowable<List<Translation>> getFavorites() {
        return mRepository.getFavorites();
    }

    @NonNull
    public Single<Translation> getTranslation(Translation translationInProgress) {
        final String processedText = translationInProgress.getInputText().trim();
        if (processedText.isEmpty()) {
            return Single.just(translationInProgress);
        }
        return mTranslateApi.translate(translationInProgress);
    }

    @NonNull
    public Completable addTranslation(Translation translation) {
        return mRepository.insertTranslation(translation);
    }

    @NonNull
    public Completable toggleFavorite(Translation translation) {
        translation.setFavorite(!translation.isFavorite());
        return mRepository.updateTranslation(translation);
    }

    @NonNull
    public Single<List<Language>> getSupportedLanguages() {
        List<Language> test = new ArrayList<>();
        test.add(new Language("ru", "Russian"));
        test.add(new Language("en", "English"));
        test.add(new Language("zh", "Chinese"));
        test.add(new Language("la", "Latin"));
        return Single.just(test);
    }

    public Completable clearHistory() {
        return mRepository.clearHistory();
    }
}

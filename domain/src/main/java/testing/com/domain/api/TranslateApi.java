package testing.com.domain.api;

import io.reactivex.Single;
import testing.com.domain.model.Translation;

public interface TranslateApi {

    Single<Translation> translate(Translation translation);
}

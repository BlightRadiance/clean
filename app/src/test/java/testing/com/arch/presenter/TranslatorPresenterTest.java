package testing.com.arch.presenter;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import testing.com.arch.TestAppComponent;
import testing.com.arch.TestComponentRule;
import testing.com.arch.view.TranslatorView$$State;
import testing.com.arch.di.AppComponent;
import testing.com.common.TrampolineSchedulerProviderImpl;
import testing.com.domain.interactor.TranslatorInteractor;
import testing.com.domain.model.Language;
import testing.com.domain.model.Translation;

public class TranslatorPresenterTest {

    @Rule
    public TestComponentRule testComponentRule = new TestComponentRule(getTestAppComponent());

    @Mock
    TranslatorInteractor mInteractor;
    @Mock
    TranslatorView$$State translatorViewState;

    private TranslatorPresenter mPresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mPresenter = new TranslatorPresenter();
        mPresenter.setViewState(translatorViewState);
    }

    @Test
    public void shouldShowTranslationHistoryAndSupportedLanguages() {
        final List<Translation> history = new ArrayList<>();
        final List<Language> languages = new ArrayList<>();

        Mockito.when(mInteractor.getTranslationHistory())
                .thenReturn(Flowable.just(history));
        Mockito.when(mInteractor.getSupportedLanguages())
                .thenReturn(Single.just(languages));

        mPresenter.onFirstViewAttach();

        Mockito.verify(mInteractor).getTranslationHistory();
        Mockito.verify(mInteractor).getSupportedLanguages();
        Mockito.verify(translatorViewState).showTranslationHistory(history);
        Mockito.verify(translatorViewState).showSupportedLanguages(languages);
    }

    @Test
    public void shouldShowTranslationHistoryAndSupportedLanguagesError() {
        Mockito.when(mInteractor.getTranslationHistory())
                .thenReturn(Flowable.error(new Exception()));
        Mockito.when(mInteractor.getSupportedLanguages())
                .thenReturn(Single.error(new Exception()));

        mPresenter.onFirstViewAttach();

        Mockito.verify(mInteractor).getTranslationHistory();
        Mockito.verify(mInteractor).getSupportedLanguages();
        Mockito.verify(translatorViewState).showTranslationHistoryError(Mockito.anyString());
        Mockito.verify(translatorViewState).showError(Mockito.anyString());
    }

    @Test
    public void shouldRequestTranslation() {
        final String someText = "Some text";
        final Translation translation = new Translation(someText);

        Mockito.when(mInteractor.getTranslation(translation))
                .thenReturn(Single.just(translation));

        mPresenter.onTextToTranslateChanged(someText);
        mPresenter.requestTranslation();

        InOrder inOrder = Mockito.inOrder(mInteractor, translatorViewState);
        inOrder.verify(translatorViewState).setTranslationInProgress(true);
        inOrder.verify(mInteractor).getTranslation(translation);
        inOrder.verify(translatorViewState).setTranslationInProgress(false);
        inOrder.verify(translatorViewState).showTranslation(translation);
    }

    @Test
    public void shouldRequestTranslationError() {
        final String someText = "Some text";
        final Translation translation = new Translation(someText);

        Mockito.when(mInteractor.getTranslation(translation))
                .thenReturn(Single.error(new Exception()));

        mPresenter.onTextToTranslateChanged(someText);
        mPresenter.requestTranslation();

        InOrder inOrder = Mockito.inOrder(mInteractor, translatorViewState);
        inOrder.verify(translatorViewState).setTranslationInProgress(true);
        inOrder.verify(mInteractor).getTranslation(translation);
        inOrder.verify(translatorViewState).setTranslationInProgress(false);
        inOrder.verify(translatorViewState).showError(Mockito.anyString());
    }

    @Test
    public void shouldAddCurrentTranslation() {
        final Translation input = new Translation("Some text");
        final Translation output = new Translation("Other text");

        Mockito.when(mInteractor.getTranslation(input))
                .thenReturn(Single.just(output));
        Mockito.when(mInteractor.addTranslation(output))
                .thenReturn(Completable.complete());

        mPresenter.onTextToTranslateChanged(input.getInputText());
        mPresenter.requestTranslation();
        mPresenter.addTranslation();

        Mockito.verify(mInteractor).addTranslation(output);
    }

    @Test
    public void shouldAddCurrentTranslationError() {
        final Translation input = new Translation("Some text");
        final Translation output = new Translation("Other text");

        Mockito.when(mInteractor.getTranslation(input))
                .thenReturn(Single.just(output));
        Mockito.when(mInteractor.addTranslation(output))
                .thenReturn(Completable.error(new Exception()));

        mPresenter.onTextToTranslateChanged(input.getInputText());
        mPresenter.requestTranslation();
        mPresenter.addTranslation();

        Mockito.verify(mInteractor).getTranslation(input);
        Mockito.verify(mInteractor).addTranslation(output);
        Mockito.verify(translatorViewState).showError(Mockito.anyString());
        Mockito.verifyNoMoreInteractions(mInteractor);
    }

    private AppComponent getTestAppComponent() {
        return new TestAppComponent() {
            @Override
            public void inject(TranslatorPresenter presenter) {
                presenter.mInteractor = mInteractor;
                presenter.mSchedulers = new TrampolineSchedulerProviderImpl();
            }
        };
    }
}
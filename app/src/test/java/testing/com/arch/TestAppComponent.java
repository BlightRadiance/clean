package testing.com.arch;

import testing.com.arch.di.AppComponent;
import testing.com.arch.presenter.FavoritesPresenter;
import testing.com.arch.view.TranslatorActivity;
import testing.com.arch.presenter.TranslatorPresenter;

public class TestAppComponent implements AppComponent {

    @Override
    public void inject(TranslatorActivity activity) {

    }

    @Override
    public void inject(TranslatorPresenter presenter) {

    }

    @Override
    public void inject(FavoritesPresenter favoritesPresenter) {

    }
}

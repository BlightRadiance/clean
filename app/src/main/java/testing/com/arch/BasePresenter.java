package testing.com.arch;

import android.support.annotation.NonNull;
import android.util.Log;

import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.MvpView;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BasePresenter <View extends MvpView> extends MvpPresenter<View> {

    private static final String TAG = BasePresenter.class.getName();

    @NonNull
    private CompositeDisposable mCompositeDisposable= new CompositeDisposable();

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() called");
        mCompositeDisposable.clear();
    }

    protected void addDisposable(Disposable disposable) {
        Log.d(TAG, "addDisposable() called with: disposable = [" + disposable + "]");
        mCompositeDisposable.add(disposable);
    }
}

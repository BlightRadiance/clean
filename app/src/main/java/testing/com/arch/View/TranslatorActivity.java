package testing.com.arch.view;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import testing.com.arch.App;
import testing.com.arch.BaseActivity;
import testing.com.arch.R;
import testing.com.arch.presenter.TranslatorPresenter;
import testing.com.common.SchedulerProvider;
import testing.com.domain.model.Language;
import testing.com.domain.model.Translation;

public class TranslatorActivity extends BaseActivity implements TranslatorView {

    private DrawerLayout mDrawerLayout;
    private RecyclerView mHistoryRecycler;
    private EditText mTextToTranslate;
    private TextView mTranslatedText;
    private Button mAddToHistoryButton;
    private Spinner mLangInput;
    private Spinner mLangOutput;

    private LanguageAdapter mLanguageAdapter;
    private TranslatorAdapter mTranslatorAdapter;

    @InjectPresenter
    TranslatorPresenter mPresenter;

    @Inject
    SchedulerProvider mSchedulers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getAppComponent().inject(this);
        setContentView(R.layout.activity_main);
        setupAddTranslationToHistoryButton();
        setupActionBar();
        setupDrawer();
        setupEditTextListeners();
        setupRecycler();
        setupSpinners();
        setupSwapSpinnersButton();
        mTranslatedText = findViewById(R.id.translatedText);
    }

    private void setupAddTranslationToHistoryButton() {
        mAddToHistoryButton = findViewById(R.id.addToHistory);
        mAddToHistoryButton.setOnClickListener(view -> {
            mPresenter.addTranslation();
            hideKeyboard(mTextToTranslate);
        });
    }

    private void setupActionBar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
        }
    }

    private void setupDrawer() {
        mDrawerLayout = findViewById(R.id.drawerLayout);
        NavigationView navigationView = findViewById(R.id.navView);
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    mDrawerLayout.closeDrawers();
                    menuItem.setChecked(true);
                    switch (menuItem.getItemId()) {
                        case R.id.nav_clear:
                            mPresenter.clearHistory();
                            break;
                        case R.id.nav_favorites:
                            FavoritesActivity.launch(this);
                            break;
                    }
                    return true;
                });
        }

    private void setupEditTextListeners() {
        mTextToTranslate = findViewById(R.id.textToTranslate);
        Observable<CharSequence> textChanges = RxTextView.textChanges(mTextToTranslate).share();
        addDisposable(textChanges
                .subscribe(text -> mPresenter.onTextToTranslateChanged(text.toString())));
        addDisposable(textChanges
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(mSchedulers.ui())
                .subscribe(text -> mPresenter.requestTranslation()));
    }

    private void hideKeyboard(EditText editText) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }
    }

    private void setupRecycler() {
        mHistoryRecycler = findViewById(R.id.history);
        mHistoryRecycler.setLayoutManager(new LinearLayoutManager(this));

        mTranslatorAdapter = new TranslatorAdapter(mPresenter);
        mHistoryRecycler.setAdapter(mTranslatorAdapter);
    }

    private void setupSpinners() {
        mLanguageAdapter = new LanguageAdapter(this, new ArrayList<>());

        mLangInput = findViewById(R.id.langInput);
        mLangInput.setAdapter(mLanguageAdapter);
        mLangInput.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mPresenter.setFromLanguage(mLanguageAdapter.getItem(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mPresenter.setFromLanguage(null);
            }
        });

        mLangOutput = findViewById(R.id.langOutput);
        mLangOutput.setAdapter(mLanguageAdapter);
        mLangOutput.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mPresenter.setToLanguage(mLanguageAdapter.getItem(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mPresenter.setToLanguage(null);
            }
        });
    }

    private void setupSwapSpinnersButton() {
        ImageView button = findViewById(R.id.swapLanguages);
        button.setOnClickListener(view -> {
            int input = mLangInput.getSelectedItemPosition();
            int output = mLangOutput.getSelectedItemPosition();
            mLangInput.setSelection(output);
            mLangOutput.setSelection(input);
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showSupportedLanguages(List<Language> languages) {
        mLanguageAdapter.clear();
        mLanguageAdapter.addAll(languages);
        mLanguageAdapter.notifyDataSetChanged();
    }

    @Override
    public void showTranslationHistory(List<Translation> translations) {
        mTranslatorAdapter.setData(translations);
        mTranslatorAdapter.notifyDataSetChanged();
    }

    @Override
    public void showTranslationHistoryError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setTranslationHistoryFilter(String filter) {
        mTranslatorAdapter.setFilter(filter);
        mTranslatorAdapter.notifyDataSetChanged();
    }

    @Override
    public void showTranslation(Translation translation) {
        mTranslatedText.setText(translation.getOutputText());
    }

    @Override
    public void clearCurrentTranslation() {
        mTextToTranslate.setText("");
        mTranslatedText.setText("");
    }

    @Override
    public void showError(String error) {
        mTranslatedText.setText(error);
    }

    @Override
    public void setTranslationInProgress(boolean inProgress) {
        if (inProgress) {
            mTranslatedText.setText("...");
            mAddToHistoryButton.setText("...");
            mAddToHistoryButton.setEnabled(false);
        } else {
            mAddToHistoryButton.setText("+");
            mAddToHistoryButton.setEnabled(true);
        }
    }
}

package testing.com.arch.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.List;

import testing.com.arch.BaseActivity;
import testing.com.arch.R;
import testing.com.arch.presenter.FavoritesPresenter;
import testing.com.domain.model.Translation;

public class FavoritesActivity extends BaseActivity implements FavoritesView {

    private TranslatorAdapter mTranslatorAdapter;

    @InjectPresenter
    FavoritesPresenter mFavoritesPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setDisplayShowHomeEnabled(true);
        }

        RecyclerView historyRecycler = findViewById(R.id.history);
        historyRecycler.setLayoutManager(new LinearLayoutManager(this));
        mTranslatorAdapter = new TranslatorAdapter(mFavoritesPresenter);
        historyRecycler.setAdapter(mTranslatorAdapter);
    }

    @Override
    public void showFavorites(List<Translation> translations) {
        mTranslatorAdapter.setData(translations);
        mTranslatorAdapter.notifyDataSetChanged();
    }

    @Override
    public void showFavoritesError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public static void launch(Context context) {
        Intent intent = new Intent(context, FavoritesActivity.class);
        if (!(context instanceof Activity)) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        context.startActivity(intent);
    }
}

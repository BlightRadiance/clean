package testing.com.arch.view;

import com.arellomobile.mvp.MvpView;

import java.util.List;

import testing.com.domain.model.Language;
import testing.com.domain.model.Translation;

public interface TranslatorView extends MvpView {

    void showSupportedLanguages(List<Language> languages);

    void showTranslationHistory(List<Translation> translations);
    void showTranslationHistoryError(String error);
    void setTranslationHistoryFilter(String filter);

    void setTranslationInProgress(boolean inProgress);
    void showTranslation(Translation translation);
    void clearCurrentTranslation();

    void showError(String error);
}
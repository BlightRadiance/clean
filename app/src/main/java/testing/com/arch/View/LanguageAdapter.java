package testing.com.arch.view;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.List;

import javax.annotation.ParametersAreNonnullByDefault;

import testing.com.domain.model.Language;

@ParametersAreNonnullByDefault
class LanguageAdapter extends ArrayAdapter<Language> {

    LanguageAdapter(Context context, List<Language> objects) {
        super(context, android.R.layout.simple_spinner_item, objects);
    }
}

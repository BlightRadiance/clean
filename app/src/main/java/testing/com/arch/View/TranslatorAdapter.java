package testing.com.arch.view;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import testing.com.arch.R;
import testing.com.arch.presenter.HistoryPresenter;
import testing.com.common.Utils;
import testing.com.domain.model.Translation;

public class TranslatorAdapter extends RecyclerView.Adapter<TranslatorAdapter.TranslationViewHolder> {

    @NonNull
    private List<Translation> mData = new ArrayList<>();
    @NonNull
    private List<Translation> mFilteredData = new ArrayList<>();

    @NonNull
    private final HistoryPresenter mPresenter;

    @Nullable
    private String mFilter = null;

    TranslatorAdapter(@NonNull HistoryPresenter presenter) {
        mPresenter = presenter;
    }

    public void setData(@NonNull List<Translation> data) {
        mData = data;
        updateFilteredData();
    }

    public void setFilter(@Nullable String filter) {
        mFilter = filter;
        updateFilteredData();
    }

    private void updateFilteredData() {
        if (Utils.isEmpty(mFilter)) {
            return;
        }
        mFilteredData.clear();
        for (Translation translation : mData) {
            if (translation.getInputText().contains(mFilter)
                    || translation.getOutputText().contains(mFilter)) {
                mFilteredData.add(translation);
            }
        }
    }

    @NonNull
    @Override
    public TranslationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.translation_list_item, parent, false);
        return new TranslationViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull TranslationViewHolder holder, int position) {
        List<Translation> source = chooseSource();
        if (position >= source.size()) {
            return;
        }
        holder.set(source.get(position));
    }

    @Override
    public int getItemCount() {
        return chooseSource().size();
    }

    @NonNull
    private List<Translation> chooseSource() {
        if (!Utils.isEmpty(mFilter)) {
            return mFilteredData;
        }
        return mData;
    }

    public class TranslationViewHolder extends RecyclerView.ViewHolder {

        @NonNull
        public TextView langInput;
        @NonNull
        public TextView langOutput;
        @NonNull
        public TextView inputText;
        @NonNull
        public TextView outputText;
        @NonNull
        public ImageView isFavorite;

        private TranslationViewHolder(View itemView) {
            super(itemView);
            langInput = itemView.findViewById(R.id.langInput);
            langOutput = itemView.findViewById(R.id.langOutput);
            inputText = itemView.findViewById(R.id.inputText);
            outputText = itemView.findViewById(R.id.outputText);
            isFavorite = itemView.findViewById(R.id.favoriteButton);
        }

        private void set(@NonNull Translation translation) {
            langInput.setText(translation.getFromLanguageId());
            langOutput.setText(translation.getToLanguageId());
            inputText.setText(translation.getInputText());
            outputText.setText(translation.getOutputText());
            setFavorite(translation.isFavorite());
            isFavorite.setOnClickListener(view ->
                    mPresenter.toggleFavorite(translation));
        }

        private void setFavorite(boolean favorite) {
            isFavorite.setImageResource(favorite ? R.drawable.ic_favorite_filled
                                                 : R.drawable.ic_favorite_empty);
        }
    }
}

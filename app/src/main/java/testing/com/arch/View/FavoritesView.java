package testing.com.arch.view;

import com.arellomobile.mvp.MvpView;

import java.util.List;

import testing.com.domain.model.Translation;

public interface FavoritesView extends MvpView {

    void showFavorites(List<Translation> translations);
    void showFavoritesError(String error);
}

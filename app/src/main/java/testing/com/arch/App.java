package testing.com.arch;

import android.app.Application;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import testing.com.arch.di.AppComponent;
import testing.com.arch.di.CommonModule;
import testing.com.arch.di.DaggerAppComponent;
import testing.com.data.di.RepositoryModule;

public class App extends Application {

    private static AppComponent sAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        sAppComponent = DaggerAppComponent.builder()
                .commonModule(new CommonModule(this))
                .repositoryModule(new RepositoryModule(this))
                .build();
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    @VisibleForTesting
    public static void setAppComponent(@NonNull AppComponent appComponent) {
        sAppComponent = appComponent;
    }
}

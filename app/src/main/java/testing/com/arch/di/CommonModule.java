package testing.com.arch.di;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.android.schedulers.AndroidSchedulers;
import testing.com.arch.AndroidSchedulerProviderImpl;
import testing.com.common.SchedulerProvider;

@Module
public class CommonModule {

    private Context mContext;

    public CommonModule(Context context) {
        mContext = context;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return mContext;
    }

    @Provides
    @Singleton
    public SchedulerProvider provideSchedulers() {
        return new AndroidSchedulerProviderImpl();
    }
}

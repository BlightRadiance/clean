package testing.com.arch.di;

import javax.inject.Singleton;

import dagger.Component;
import testing.com.arch.presenter.FavoritesPresenter;
import testing.com.arch.view.TranslatorActivity;
import testing.com.arch.presenter.TranslatorPresenter;
import testing.com.data.di.RepositoryModule;

@Singleton
@Component(modules = {CommonModule.class, RepositoryModule.class})
public interface AppComponent {

    void inject(TranslatorActivity activity);

    void inject(TranslatorPresenter presenter);

    void inject(FavoritesPresenter favoritesPresenter);
}

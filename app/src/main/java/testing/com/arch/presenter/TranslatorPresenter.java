package testing.com.arch.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import testing.com.arch.App;
import testing.com.arch.BasePresenter;
import testing.com.arch.view.TranslatorView;
import testing.com.common.SchedulerProvider;
import testing.com.common.Utils;
import testing.com.domain.interactor.TranslatorInteractor;
import testing.com.domain.model.Language;
import testing.com.domain.model.Translation;

@InjectViewState
@ParametersAreNonnullByDefault
public class TranslatorPresenter extends BasePresenter<TranslatorView> implements HistoryPresenter {

    private static final String TAG = TranslatorPresenter.class.getName();

    @Inject
    @NonNull
    TranslatorInteractor mInteractor;

    @Inject
    @NonNull
    SchedulerProvider mSchedulers;

    @NonNull
    private Translation mCurrentTranslation = new Translation();

    @Nullable
    private Disposable mPendingTranslationRequest;

    public TranslatorPresenter() {
        App.getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        Log.d(TAG, "onFirstViewAttach() called");
        addDisposable(mInteractor.getTranslationHistory()
                .observeOn(mSchedulers.ui())
                .subscribe(translations -> getViewState().showTranslationHistory(translations),
                        throwable -> getViewState().showTranslationHistoryError("Error: " + throwable.getMessage())));
        addDisposable(mInteractor.getSupportedLanguages()
                .observeOn(mSchedulers.ui())
                .subscribe(languages -> getViewState().showSupportedLanguages(languages),
                        throwable -> getViewState().showError("Error: " + throwable.getMessage())));
    }

    private boolean shouldTryToTranslate() {
        return !Utils.isEmpty(mCurrentTranslation.getInputText());
    }

    public void requestTranslation() {
        Log.d(TAG, "requestTranslation() called");
        if (!shouldTryToTranslate()) {
            return;
        }
        mPendingTranslationRequest = mInteractor
                .getTranslation(mCurrentTranslation)
                .observeOn(mSchedulers.ui())
                .subscribe(translation -> {
                    mCurrentTranslation = translation;
                    getViewState().setTranslationInProgress(false);
                    getViewState().showTranslation(translation);
                }, throwable -> {
                    getViewState().setTranslationInProgress(false);
                    getViewState().showError("Failed to get translation: " + throwable.getMessage());
                });
        addDisposable(mPendingTranslationRequest);
    }

    public void onTextToTranslateChanged(String text) {
        Log.d(TAG, "onTextToTranslateChanged() called");
        mCurrentTranslation.setInputText(text);
        if (!Utils.isEmpty(text)) {
            getViewState().setTranslationInProgress(true);
        } else {
            getViewState().setTranslationInProgress(false);
            mCurrentTranslation.setOutputText("");
            getViewState().showTranslation(mCurrentTranslation);
        }
        if (mPendingTranslationRequest != null) {
            mPendingTranslationRequest.dispose();
        }
        getViewState().setTranslationHistoryFilter(text);
    }

    public void addTranslation() {
        Log.d(TAG, "addTranslation() called");
        if (!shouldTryToTranslate()) {
            return;
        }
        addDisposable(mInteractor.addTranslation(mCurrentTranslation)
            .observeOn(mSchedulers.ui())
            .subscribe(() -> {},
                    throwable -> getViewState().showError("Failed to save translation: " + throwable.getMessage())));
        getViewState().clearCurrentTranslation();
    }

    public void setFromLanguage(@Nullable Language from) {
        Log.d(TAG, "setFromLanguage() called with: from = [" + from + "]");
        mCurrentTranslation.setFromLanguage(from);
        if (shouldTryToTranslate()) {
            getViewState().setTranslationInProgress(true);
            requestTranslation();
        }
    }

    public void setToLanguage(@Nullable Language to) {
        Log.d(TAG, "setToLanguage() called with: to = [" + to + "]");
        mCurrentTranslation.setToLanguage(to);
        if (shouldTryToTranslate()) {
            getViewState().setTranslationInProgress(true);
            requestTranslation();
        }
    }

    public void clearHistory() {
        addDisposable(mInteractor.clearHistory().subscribe());
    }

    @Override
    public void toggleFavorite(Translation translation) {
        addDisposable(mInteractor.toggleFavorite(translation).subscribe());
    }
}

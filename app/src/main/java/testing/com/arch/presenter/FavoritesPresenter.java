package testing.com.arch.presenter;

import com.arellomobile.mvp.InjectViewState;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;

import testing.com.arch.App;
import testing.com.arch.BasePresenter;
import testing.com.arch.view.FavoritesView;
import testing.com.common.SchedulerProvider;
import testing.com.domain.interactor.TranslatorInteractor;
import testing.com.domain.model.Translation;

@InjectViewState
@ParametersAreNonnullByDefault
public class FavoritesPresenter extends BasePresenter<FavoritesView> implements HistoryPresenter {

    @Inject
    TranslatorInteractor mInteractor;

    @Inject
    SchedulerProvider mSchedulers;

    public FavoritesPresenter() {
        App.getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        addDisposable(mInteractor.getFavorites()
                .observeOn(mSchedulers.ui())
                .subscribe(translations -> getViewState().showFavorites(translations),
                        throwable -> getViewState().showFavoritesError("Error: " + throwable.getMessage())));
    }

    @Override
    public void toggleFavorite(Translation translation) {
        addDisposable(mInteractor.toggleFavorite(translation).subscribe());
    }
}

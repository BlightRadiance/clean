package testing.com.arch.presenter;

import testing.com.domain.model.Translation;

public interface HistoryPresenter {

    void toggleFavorite(Translation translation);
}
